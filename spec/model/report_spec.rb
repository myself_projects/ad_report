require 'spec_helper'
require_relative '../../app/integrations/libring/gateway'

# require_relative '/app/models/report'

RSpec.describe 'Report' do
  context "Running the script" do
    it 'Passing valid dates' do
      response = Integrations::Libring::Gateway.new(start_date: '2018-05-01', end_date: '2018-05-05').request_report
      expect(response.size).not_to eq(0)
    end
  end

  it 'Passing bad dates' do
    response = Integrations::Libring::Gateway.new(start_date: '20-05-01', end_date: '2018-05-05').request_report
    expect(response).to be_nil
  end



end
