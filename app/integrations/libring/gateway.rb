require_relative '../../business/base'

module Integrations
  module Libring
    class Gateway < Business::Base
      attr_reader :start_date, :end_date

      def initialize(overrides = {})
        @base_url = ENV['LIBRING_URL']
        @start_date = overrides.fetch(:start_date)
        @end_date = overrides.fetch(:end_date)
      end

      def request_report
        response = get

        return puts 'No reports was found' unless response.code.eql?(201)

        @connections = response['connections']

        next_request(response['next_page_url']) if next_url?(response)

        @connections.flatten
      end

      private

      def get
        puts 'Searching reports...'

        HTTParty.get("#{@base_url}/get?period=custom_date&start_date=#{@start_date}&end_date=#{@end_date}&data_type=adnetwork&group_by=#{groups}",
                     headers: libring_header_token)
      end

      def groups
        'connection,app,platform,country'
      end

      def next_request(url)
        response = HTTParty.get(url, headers: libring_header_token)
        @connections << response['connections']
        next_request(response['next_page_url']) if next_url?(response)
      end

      def next_url?(response)
        response.key?('next_page_url') && !response['next_page_url'].size.zero?
      end

      def libring_header_token
        { Authorization: "Token #{ENV['LIBRING_TOKEN']}" }
      end
    end
  end
end
