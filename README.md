# README

* Ruby version
  - 2.5.0
* System dependencies
  - Database: PostgreSQL

* Configuration
  - RUN ``` bundle install``` to install all required gems
  - Setting in `database.yml` your database informations
  - Copy the file `.env.development.sample` named the copy as `.env.development` and setting Libring Token in it.

* Database creation
  - ``` rake db:create db:migrate ```

* Test suite
  - RUN ``` bundle exec rspec ```

* Deployment instructions
  - To start the script you must run ``` ruby main.rb ```, then the script will ask you to insert
  both start and end date to search in Libring API.
