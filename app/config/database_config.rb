require 'active_record'
require_relative '../models/report'

class DatabaseConfig

  def self.db_configuration
    db_configuration_file = File.join(File.expand_path('..', __FILE__), '../..', 'db', 'config.yml')
    YAML.load(File.read(db_configuration_file))
  end
end
