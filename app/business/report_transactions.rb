require_relative '../config/database_config'
require_relative 'translator'

class ReportTransactions < DatabaseConfig
  ActiveRecord::Base.establish_connection(db_configuration['development'])

  def self.save(connections_raw)
    @connections_raw = connections_raw

    puts 'Saving reports in database...'
    @connections_raw.each do |connection|
      execute(Business::Translator.translate(connection))
    end
    success
  end

  def self.execute(connection)
    Report.create(connection) #save
  end

  def self.success
    puts "* Was saved #{@connections_raw.count} transactions in data base *"
  end
end
