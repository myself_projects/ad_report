class CreateReports < ActiveRecord::Migration[5.2]
  def change
    create_table :reports do |t|
      t.string :connection, null: false
      t.string :app, null: false
      t.string :platform, null: false
      t.string :country, null: false
      t.float :impressions
      t.float :ad_revenue
      t.date :date, null: false

      t.timestamps
    end
  end
end
