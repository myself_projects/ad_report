module Business
  class Translator

    def self.translate(raw_report)
      {
        'connection': raw_report['connection'],
        'app': raw_report['app'],
        'platform': raw_report['platform'],
        'country': raw_report['country'],
        'impressions': raw_report['impressions'],
        'ad_revenue': raw_report['ad_revenue'],
        'date': Time.parse(raw_report['date'])
      }
    end
  end
end
