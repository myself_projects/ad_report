require_relative 'app/integrations/libring/gateway'
require_relative 'app/business/report_transactions'

class Main
  def start
    puts "Welcome! \nYou started the AdReport extractor.\n"

    receive_dates
    extract_reports

    puts 'Bye bye'
  rescue StandardError => e
    puts '********** ERROR ***********'
    puts "Something went wrong => #{e}"
  end

  private

  def receive_dates
    puts 'Please insert the start date range (YYYY-MM-DD):'
    @start_date = format_date(gets.chomp)

    puts 'Please insert the end date range (YYYY-MM-DD):'
    @end_date = format_date(gets.chomp)
  end

  def extract_reports
    response = Integrations::Libring::Gateway.new(start_date: @start_date, end_date: @end_date).request_report
    ReportTransactions.save(response)
  end

  def format_date(date)
    Time.parse(date).strftime('%F')
  end
end

Main.new.start
